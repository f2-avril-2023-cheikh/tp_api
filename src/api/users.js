import axios from "axios";

const API_URL = "https://jsonplaceholder.typicode.com/users";

export function getUsers() {
  try {
    const response = axios.get(API_URL);
    console.log(response);
  } catch (error) {
    console.log(error);
  }
}
