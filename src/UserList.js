import React from "react";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";

const UserList = () => {
  const [listOfUser, setListOfUser] = useState([]);
  const API_URL = "https://jsonplaceholder.typicode.com/users";

  useEffect(() => {
    axios
      .get(API_URL)
      .then((r) => setListOfUser(r.data))
      .catch((error) => console.log(error));
  }, []);

  console.log(listOfUser);

  return (
    <div>
      {listOfUser.map((user) => (
        <ul>
          <li>{user.name} </li>
          <li>{user.username}</li>
          <li>{user.email}</li>
        </ul>
      ))}
    </div>
  );
};

export default UserList;
